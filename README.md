# VoxCity

As funcionalidades que estarão disponibilizados são: Cliente e Técnico.

Cliente: Abertura de Ordens de Serviço, Visualização das ordens abertas por ele e os status de solução criadas pelo Técnico;

Técnico: Terá na tela inicial as ordens de serviço abertas, juntamente com o nome de quem abriu, data e hora. Poderá alterar o status da ordem(em solução, ordem transferida para departamento tal), e encerrar informando a solução tomada, hora e data. Terá também as mesmas funcionalidades do cliente/cidadão, para abrir solicitações e terá uma visão dos usuários que fizeram cadastro no sistema.

# Documentação

* [Guia do Desenvolvedor] (https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md)
* [Guia do Usuário] (https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md)
