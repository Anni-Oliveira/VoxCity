# Guia do Desenvolvedor

## Sumário
* [Introdução] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#introdu%C3%A7%C3%A3o)
* [Tecnologias empregadas] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#tecnologias-empregadas)
    * [JavaScript] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#javascript)
    * [HTML5] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#html5)
    * [CSS3] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#css3)
    * [Gitlab] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#gitlab)
* [Instalando o Ambiente de Desenvolvimento] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#instalando-o-ambiente-de-desenvolvimento)
    * [Ubuntu/Linux] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#ubuntulinux)
    * [Windows] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#windows)
* [Como submeter contribuições] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#como-submeter-contribui%C3%A7%C3%B5es)
    * [Passo 1 - Clonar o projeto] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#passo-1-clonar-o-projeto)
    * [Passo 2 - Branch (Ramo)] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#passo-2-branch-ramo)
    * [Passo 3 - Submeter alterações] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#passo-3-submeter-altera%C3%A7%C3%B5es)
    * [Passo 4 - Criando um Merge Request] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#passo-4-criando-um-merge-request)
* [Conhecendo o sistema](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#conhecendo-o-sistema)
    * [Login](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#login)
    * [Cadastro](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#cadastro)
    * [Solicitar Serviço](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#solicitar-servi%C3%A7o)
    * [Minhas Solicitações](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#minhas-solicita%C3%A7%C3%B5es)
    * [Opções para Administradores](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#op%C3%A7%C3%B5es-para-administradores)
    * [Dashboard - Lista de Solicitações](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#dashboard-lista-de-solicita%C3%A7%C3%B5es)
    * [Users Management - Usuários Cadastrados](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#users-management-usu%C3%A1rios-cadastrados)
* [Diagramas do Sistema](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#diagramas-do-sistema)
    * [Diagrama de Caso de Uso](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#diagrama-de-caso-de-uso)
    * [Diagrama de Atividade](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#diagrama-de-atividade)
    * [Diagrama de Sequência](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#diagrama-de-sequ%C3%AAncia)
        * [Cadastro](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#cadastro-1)
        * [Login](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#login-1)
        * [Solicitação](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#solicita%C3%A7%C3%A3o)
    * [Diagrama de Componentes](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaDesenvolvedor.md#diagrama-de-componentes)

## Introdução
<p> Este documento tem como objetivo ser um guia completo para entendimento do código usado para a criação do site de chat online
há partir de programação para WebSockets e servidor IRC. Para compreender melhor eh necessário o conhecimento básico de <strong> WebSockets,
JavaScript, HTML5, CSS3 </strong>. <br />

</p>

## Tecnologias empregadas

### JavaScript
<p> JavaScript é uma linguagem de programação interpretada. Foi originalmente implementada como parte dos navegadores web para que scripts pudessem ser 
executados do lado do cliente e interagissem com o usuário sem a necessidade deste script passar pelo servidor, controlando o navegador, 
realizando comunicação assíncrona e alterando o conteúdo do documento exibido. <br />

</p>
*Origem: Wikipédia, a enciclopédia livre.*

### HTML5
<p> HTML5 (Hypertext Markup Language, versão 5) é uma linguagem para estruturação e apresentação de conteúdo para a World Wide Web e é 
uma tecnologia chave da Internet originalmente proposto por Opera Software. É a quinta versão da linguagem HTML. Esta nova versão traz 
consigo importantes mudanças quanto ao papel do HTML no mundo da Web, através de novas funcionalidades como semântica e acessibilidade.<br />

</p>
*Origem: Wikipédia, a enciclopédia livre.*

### CSS3
<p> CSS3 é a segunda mais nova versão das famosas Cascading Style Sheets (ou simplesmente CSS), onde se define estilos para páginas web 
com efeitos de transição, imagem, e outros, que dão um estilo novo às páginas Web 2.0 em todos os aspectos de design do layout.<br />
</p>

*Origem: Wikipédia, a enciclopédia livre.*

### Gitlab 
<p> Gitlab é um serviço de repositório, gestão de projetos, códigos e colaboração, existem vários repositórios como este, 
são nestes serviços que você vai fazer o upload do seu código fonte e gerir tudo que é produzido. <br />

Informações de instalação e utilização estão disponiveis no site <strong> http://devopslab.com.br/instalacao-do-gitlab-e-comandos-do-git/ </strong>

</p>

## Instalando o Ambiente de Desenvolvimento

<p> Para a instalação do o ambiente de desenvolvimento, será necessário instalar a ferramenta Meteor e um editor de códigos, o que foi usado neste projeto Visual
Studio Code, mas caso preferir, pode usar outro editor. <br /><br />

Site Visual Studio Code: <strong>https://code.visualstudio.com/download</strong>
</p>

### Ubuntu/Linux
<p> Abra o terminal de sua máquina e digite <strong> curl https://install.meteor.com/ | sh </strong> para instalar o Meteor.
Após isso, como foi dito, pode-se fazer a instalação do Visual Studio Code. 

</p>

### Windows
<p> Primeiro tem que abrir o CMD(prompt de comando) e instalar o Chocolatey então execute este comando usando um prompt de comando do Administrador: <br />
<strong>choco install meteor</strong>. 

</p>

## Como Submeter Contribuições
<p> Como já foi informado, o projeto está alocado no gitlab, uma plataforma gestão de projetos, códigos e colaboração. Para submeter contribuições
é necessário que você faça um <strong>fork</strong> no projeto, para que tenha uma "cópia" em sua conta. Assim poderá prosseguir com os passos à seguir: 
</p>

### Passo 1 - Clonar o projeto
<p> Deve ser feita a instalação do <strong>git</strong> em sua máquina para prosseguir com os passos. 
</p>

* **Comandos** : 
    * git clone git@gitlab.com:Anni-Oliveira/VoxCity.git - SSH
    * git clone https://gitlab.com/Anni-Oliveira/VoxCity.git - HTTPS
* **Observação**: Por padrão é usado a versão SSH, porém algumas configurações de rede pode impedir o donwload em SSH. Caso isso ocorrer, pode-se
usar o HTTPS.

### Passo 2 - Branch (Ramo)
<p> Não é permitido alterar o documento diretamente, ou seja, no projeto principal, por isso é necessário fazer um <strong>Fork</strong>, 
então quando for enviar as alterações para sua conta no gitlab, será preciso criar uma branch que é uma ramificação do projeto. 
</p>

* **Comandos**: 
    * git branch nome-para-a-branch - Cria uma nova branch 
    * git branch - Lista as branchs já criadas no projeto. A branch usada será marcada com um asterisco.
    * git checkout nome-para-a-branch - Altera a branch usada para a informada.

### Passo 3 - Submeter alterações
<p> Após alterar os arquivos na branch criada, será necessário dar um push que significa "enviar ou empurrar" para a sua conta no gitlab. 
</p>

* **Comando**: git push origin nome-para-a-branch 

### Passo 4 - Criando um Merge Request
<p> O Merge resquest é o passo final, onde você enviará ao projeto original as alterações feitas no projeto em sua conta. 
Faça então o login na sua conta no gitlab, e então acessar a opção de <strong>Merge Request</strong> que pode ser encontrado no canto superior à direita
ou na lista à esquerda. Escolher o projeto que deseja e a branch com as alterações feitas, então preencher com as informações que desejar e enviar. <br /><br />

<strong> Pronto! Suas alterções foram enviadas a conta original! </strong>
</p>

## Conhecendo o sistema

<p> Segue alguns <strong>prints do sistema</strong> e breve descrição. Essa parte do documento está sujeita a alterações caso o sistema for atualizado. 
</p>

### Login

![imagem](Documentação/imagens-telas/login.png)

**********

### Cadastro

![imagem](Documentação/imagens-telas/cadastro-usuário.png)

<p> Todos os campos do cadastro devem ser preenchidas. Os pontos principais são: <strong>E-mail e CPF</strong>. Através deles fica mais fácil a identificação e verificação pois assim evitamos fraudes de informação e limitação de usuários (um usuário não pode cadastrar-se 2 vezes). 
</p>

**********

### Solicitar serviço

![imagem](Documentação/imagens-telas/solicitar-servico-admin.png)

<p> Assim como um usuário comum, é possivel que o administrador faça solicitações de serviço ao governo. A principal diferença está na barra da esquerda, que só é mostrada a administradores do sistema. 
</p>

*******

### Minhas solicitações

![imagem](Documentação/imagens-telas/lista-solicitações-usuario.png)

<p> Essa é uma pequena lista das solicitações feitas abertas em sua conta. 
</p>

******

### Opções para Administradores

![imagem](Documentação/imagens-telas/opcoes-adicionais-admin.png)

* **Dashboard**: Mostra todas as solicitações abertas no sistema;
* **Users Management**: Mostra a lista dos usuários cadastrados.

****

### Dashboard - Lista de Solicitações

![imagem](Documentação/imagens-telas/lista-servico-admin.png)

<p> Essa lista é de <strong>TODAS</strong> as solicitações abertas pelos usuários cadastrados no sistema, independente de ser usuário comum ou administrador. Essa opção é disponivel apenas para <strong>administradores</strong>.
</p>

****

### Users Management - Usuários cadastrados

![imagem](Documentação/imagens-telas/lista-usuario.png)

<p> Assim como foi dito, esta opção é disponibilizada apenas para <strong>administradores</strong> do sistema, através dele é possivel ver usuários cadastrados no sistema, assim também quais são os usuários "comuns" e os administradores. Na proxima versão do sistema, estará disponivel a opção de tornar um usuário em administrador, por enquanto essa funcionalidade ainda está em desenvolvimento. 
</p>

## Diagramas do Sistema

### Diagrama de Caso de Uso

<p> O <strong>diagrama de caso de uso</strong> descreve a funcionalidade proposta para um novo sistema que será projetado, é uma excelente ferramenta para o levantamento dos requisitos funcionais do sistema. 
</p>
*Origem: Wikipédia, a enciclopédia livre.*

![imagem](Documentação/Diagramas/Caso-de-Uso-Sistema.jpg)

### Diagrama de Atividade

<p> O <strong>Diagrama de atividade</strong> é um diagrama definido pela Linguagem de Modelagem Unificada (UML), e representa os fluxos conduzidos por processamentos. É essencialmente um gráfico de fluxo, mostrando o fluxo de controle de uma atividade para outra.

</p>
*Origem: Wikipédia, a enciclopédia livre.*

![imagem](Documentação/Diagramas/Diagrama-Atividade-Sistema-Usuário.jpg)


### Diagrama de Sequência

<p><strong>Diagrama de sequência</strong>(ou Diagrama de Sequência de Mensagens) é um diagrama usado em UML (Unified Modeling Language), representando a sequência de processos (mais especificamente, de mensagens passadas entre objetos) num programa de computador.
</p>
*Origem: Wikipédia, a enciclopédia livre.*

#### Cadastro
![imagem](Documentação/Diagramas/Diagrama-Sequencia-Cadastro.jpg)

#### Login
![imagem](Documentação/Diagramas/Diagrama-Sequencia-Login.jpg)

#### Solicitação
![imagem](Documentação/Diagramas/Diagrama-Sequencia-Solicitação.jpg)

### Diagrama de Componentes

<p><strong>Diagrama de componentes</strong> da UML ilustra como as classes deverão se encontrar organizadas através da noção de componentes de trabalho. Por exemplo, pode-se explicitar, para cada componente, qual das classes que ele representa. É utilizado para: Modelar os dados do código fonte, do código executável do software.
</p>

*Origem: Wikipédia, a enciclopédia livre.*

![imagem](Documentação/Diagramas/Diagrama-Componentes-VoxCity.jpg)
