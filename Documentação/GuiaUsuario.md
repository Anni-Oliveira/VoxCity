# Guia do Usuário

## Sumário

  * [Introdução](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#introdu%C3%A7%C3%A3o)
  * [Conhecendo o Site](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#conhecendo-o-site)
    * [Tela Inicial](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#tela-inicial)
    * [Login](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#login)
    * [Cadastro](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#cadastro)
    * [Tela após login](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#tela-ap%C3%B3s-login)
    * [Solicitar Serviço](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#solicitar-servi%C3%A7o)
    * [Minhas solicitações](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#minhas-solicita%C3%A7%C3%B5es)
  * [Requisitos do Sistema](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#requisitos-do-sistema)
    * [Requisitos para cadastro](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#requisitos-para-cadastro)
    * [Requistos de software e hardware](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#requisitos-de-software-e-hardware)
    

## Introdução
<p>O site do <strong>VoxCity</strong> foi desenvolvido no intuito de levar ao novo patamar a comunicação entre governo e cidadão. Percebemos que apesar da grande quantidade de meios de comunicação disponíveis, há uma grande deficiência no que concerne a comunicação entre cidadão e governo. Tal deficiência é principalmente percebida quando se deseja informar um problema que esteja ocorrendo na sua rua e afligindo a comunidade local, ou um problema individual. O cidadão se depara com meios burocráticos e complexos sempre que deseja solicitar um serviço e, devido a tal complexidade acaba desistindo de correr atrás de direitos básicos. <br /><br />

Então foi pensado uma meio mais rápido e prático que poderá ser usada por ambos cidadão e governo. Um site de solicitações de serviços que poderá ser usada por qualquer pessoa cuja a cidade tenha aderido ao sistema. Inicialmente será para serviços <strong>"pequenos"</strong> mas importantes que podem tornar-se um empecilho na vida do cidadão. Um bom exemplo, e que será mostrado ainda neste documento, são buracos em rodovias e lixo em lotes baldios. Com esse sistema o governo verá em tempo real a solicitação aberta e onde está o problema.
</p>

## Conhecendo o site

<p> A seguir tem uma série de prints do sistema e algumas descrições que poderão ser modificadas conforme as atualizações que o sistema pode sofrer. </p>

### Tela inicial

![imagem](Documentação/imagens-telas/tela-inicial.png)

### Login

![imagem](Documentação/imagens-telas/login.png)

<p> Para fazer o <strong>login</strong> será necessário fazer o <strong>cadastro</strong> com um e-mail válido. </p>

### Cadastro

![imagem](Documentação/imagens-telas/cadastro-usuário.png)

* [**Requisitos para cadastro**](https://gitlab.com/Anni-Oliveira/VoxCity/blob/master/Documenta%C3%A7%C3%A3o/GuiaUsuario.md#requisitos-para-cadastro)

* **E-mail**: Um email válido para que o sistema possa fazer a confirmação e enviar possiveis atualizações.
* **Password**: Uma senha de no minimo 6 caracteres!
* **Nome**: Seu primeiro nome para registro.
* **SobreNome**: Para melhor identificar o usuário. 
* **CPF**: O campo mais importante, para em caso de fraude ou informação falsa, poder identificar a pessoa.


### Tela após login

![imagem](Documentação/imagens-telas/tela-usuario.png)

<p> Não eh muito diferente da tela inicial do sistema, porém terá novas opções ao lado do logout </p>

### Solicitar serviço

![imagem](Documentação/imagens-telas/solicitar-servico-usuario.png)

<p> É uma pequena lista inicial para descrição e localização do problema que o cidadão encontrou na cidade e que pode vir a ser um encomodo. </p>

* **Descrição**: Após marcar qual o tipo de problema, poderá fazer uma breve descrição da situação. Ex: Buraco aberto com a chuva, está muito grande e perigoso.
* **Rua/Avenida, Bairro/Setor e CEP**: Dados para melhor localizar o problema.


### Minhas solicitações

![imagem](Documentação/imagens-telas/lista-solicitações-usuario.png)

<p> Assim que submeter as solicitações, o usuário poderá ver e acompanhar a situação. </p>

## Requisitos do Sistema

### Requisitos para cadastro:
<p> Assim como mostrado no print, será necessário preencher <strong>TODOS</strong> os campos. </p>
<p>Para manter a segurança da informação que será enviada pelos usuários, o site terá no cadastro a solicitação do <strong> CPF </strong> do usuário, assim poderemos evitar fraudes e fazer a segurança devida das informações coletadas. Toda informação que for <strong> falsa </strong> , será informada ao orgão público, que terá um número limite de informações ditas <strong> falsas </strong>, se uma pessoa ultrapassar essas informações, terá que pagar uma taxa ao governo (que será decidida pelo mesmo).
</p>

### Requisitos de software e hardware: 
<p>O site roda nos principais navegadores disponiveis e no Internet Explorer à partir da versão 6. Infelizmente o único navegador que não roda é o 
Explorer Edge. Os requisitos dos principais navegadores (Mozilla Firefox e Google Chrome): 
</p>

**Chrome:**
* Windows:
    * Windows 7, Windows 8, Windows 8.1, Windows 10 ou versão posterior;
    * Processador Intel Pentium 4 ou posterior compatível com SSE2;

* Mac:
    * OS X Mavericks 10.9 ou versão posterior;

* Linux:
    * Debian 8+, openSUSE 13.3+, Fedora Linux 24+ ou Ubuntu 14.04+ de 64 bits;
    * Processador Intel Pentium 4 ou posterior compatível com SSE2 ;

**Firefox:**
* Windows (32 e 64 bits)
    * Sistemas operacionais: Windows 10, 8, 7, Vista, Server 2003 SP1
    * A versão 64 bits funciona no Windows 7 e posteriores.
    * Hardware mínimo: Pentium 4 ou mais recente que suporte SSE2, 512 MB de RAM, 200MB de espaço em disco

* Mac
    * Sistemas operacionais: Mac OS X 10.11 (El Capitan), 10.10 (Yosemite), 10.9 (Mavericks), 10.8 (Montain Lion), 10.7 (Lion)
    * Hardware mínimo: Computador Macintosh com processador Intel x86, 512MB de RAM, 200MB de espaço em disco

* Linux	
    * Sistema operacional: Linux
    * Requisitos de software: Observe que distribuições Linux podem fornecer pacotes com requisitos diferentes.
